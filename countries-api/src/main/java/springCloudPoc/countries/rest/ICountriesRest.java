package springCloudPoc.countries.rest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springCloudPoc.countries.rest.dto.CountryDto;

import java.util.List;

/**
 * Created by bayona on 30/08/2016.
 */
@RestController
public interface ICountriesRest {

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    CountryDto getCountry(@PathVariable String id);

    @RequestMapping(method = RequestMethod.GET)
    List<CountryDto> listCountries();

}
