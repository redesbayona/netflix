package springCloudPoc.countries.rest.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by bayona on 30/08/2016.
 */
@Data
public class CountryDto implements Serializable {

    private String id;
    private String name;
}
