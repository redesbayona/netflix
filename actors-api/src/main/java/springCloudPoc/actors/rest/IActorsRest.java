package springCloudPoc.actors.rest;

import org.springframework.web.bind.annotation.*;
import springCloudPoc.actors.rest.dto.ActorDto;

import java.util.List;

/**
 * Created by bayona on 30/08/2016.
 */
@RestController
public interface IActorsRest {

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    ActorDto getActor(@PathVariable("id") String id);

    @RequestMapping(method = RequestMethod.GET)
    List<ActorDto> listActors();

    @RequestMapping(method = RequestMethod.POST)
    void addActor(@RequestBody ActorDto actor);

}
