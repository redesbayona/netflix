package springCloudPoc.actors.rest.dto;

import lombok.Data;
import springCloudPoc.countries.rest.dto.CountryDto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by bayona on 30/08/2016.
 */
@Data
public class ActorDto implements Serializable {

    private String id;
    private String name;
    private String surname;
    private Date birth;
    private CountryDto nationality;

}
