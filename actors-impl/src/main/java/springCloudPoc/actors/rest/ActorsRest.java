package springCloudPoc.actors.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springCloudPoc.actors.rest.dto.ActorDto;
import springCloudPoc.actors.services.api.IActorsService;

import java.util.List;

/**
 * Created by bayona on 30/08/2016.
 */
@RestController
public class ActorsRest implements IActorsRest {

    @Autowired
    private IActorsService actorsService;

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ActorDto getActor(@PathVariable("id") String id) {
        return actorsService.getActor(id);
    }

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public List<ActorDto> listActors() {
        return actorsService.listActors();
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public void addActor(@RequestBody ActorDto actor) {
        actorsService.addActor(actor);
    }
}
