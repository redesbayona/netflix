package springCloudPoc.actors.repositories;

import org.springframework.data.repository.CrudRepository;
import springCloudPoc.actors.repositories.models.ActorModel;

/**
 * Created by bayona on 30/08/2016.
 */
public interface ActorsRepository extends CrudRepository<ActorModel, String> {
}
