package springCloudPoc.actors.repositories.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by bayona on 30/08/2016.
 */
@Data
@Entity
@Table(name = "actors")
public class ActorModel {

    @Id
    private String id;
    private String name;
    private String surname;
    private Date birth;
    private String countryid;
}

