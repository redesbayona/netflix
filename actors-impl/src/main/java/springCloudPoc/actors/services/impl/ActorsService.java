package springCloudPoc.actors.services.impl;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springCloudPoc.actors.clients.countries.CountriesClients;
import springCloudPoc.actors.repositories.ActorsRepository;
import springCloudPoc.actors.repositories.models.ActorModel;
import springCloudPoc.actors.rest.dto.ActorDto;
import springCloudPoc.actors.services.api.IActorsService;
import springCloudPoc.actors.services.mappers.ActorMapper;

import java.util.List;
import java.util.UUID;

/**
 * Created by bayona on 30/08/2016.
 */
@Service
public class ActorsService implements IActorsService {

    @Autowired
    private ActorsRepository actorsRepository;

    @Autowired
    private ActorMapper actorMapper;

    @Autowired
    private CountriesClients countriesClients;

    @Override
    public ActorDto getActor(String id) {
        ActorModel one = actorsRepository.findOne(id);
        ActorDto actorDto = actorMapper.mapToOuter(one);
        if (one != null) {
            actorDto.setNationality(countriesClients.getCountry(actorDto.getNationality().getId()));
        }
        return actorDto;
    }

    @Override
    public List<ActorDto> listActors() {
        Iterable<ActorModel> all = actorsRepository.findAll();
        return actorMapper.mapToOuterList(IteratorUtils.toList(all.iterator()));
    }

    @Override
    public void addActor(ActorDto actor) {
        ActorModel actorModel = actorMapper.mapToInner(actor);
        actorModel.setId(UUID.randomUUID().toString().substring(0,10));
        actorsRepository.save(actorModel);
    }
}
