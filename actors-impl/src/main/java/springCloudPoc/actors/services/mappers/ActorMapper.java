package springCloudPoc.actors.services.mappers;

import org.springframework.stereotype.Component;
import springCloudPoc.actors.repositories.models.ActorModel;
import springCloudPoc.actors.rest.dto.ActorDto;
import springCloudPoc.countries.rest.dto.CountryDto;
import springCloudPoc.webUtils.commons.mapper.IMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bayona on 30/08/2016.
 */
@Component
public class ActorMapper implements IMapper<ActorDto, ActorModel> {

    @Override
    public ActorDto mapToOuter(final ActorModel inner) {
        if (inner == null) {
            return null;
        }
        ActorDto outer = new ActorDto();
        outer.setId(inner.getId());
        outer.setName(inner.getName());
        outer.setBirth(inner.getBirth());
        String nationality = inner.getCountryid();
        outer.setNationality(new CountryDto());
        outer.getNationality().setId(nationality);
        outer.setSurname(inner.getSurname());
        return outer;
    }

    @Override public ActorModel mapToInner(final ActorDto outer) {
        if (outer == null) {
            return null;
        }
        ActorModel inner = new ActorModel();
        inner.setId(outer.getId());
        inner.setName(outer.getName());
        inner.setBirth(outer.getBirth());
        inner.setCountryid(outer.getNationality().getId());
        inner.setSurname(outer.getSurname());
        return inner;
    }

    @Override
    public List<ActorDto> mapToOuterList(final List<ActorModel> innerList) {
        if (innerList == null) {
            return null;
        }
        List<ActorDto> actors = new ArrayList<>();
        for (ActorModel model : innerList) {
            actors.add(mapToOuter(model));
        }
        return actors;
    }

    @Override
    public List<ActorModel> mapToInnerList(final List<ActorDto> outerList) {
        if (outerList == null) {
            return null;
        }
        List<ActorModel> actors = new ArrayList<>();
        for (ActorDto dto : outerList) {
            actors.add(mapToInner(dto));
        }
        return actors;
    }


}
