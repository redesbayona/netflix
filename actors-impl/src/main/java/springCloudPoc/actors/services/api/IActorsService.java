package springCloudPoc.actors.services.api;

import springCloudPoc.actors.rest.dto.ActorDto;

import java.util.List;

/**
 * Created by bayona on 30/08/2016.
 */
public interface IActorsService {

    ActorDto getActor(String id);

    List<ActorDto> listActors();

    void addActor(ActorDto actor);
}
