DROP DATABASE netflix;
CREATE DATABASE netflix;
USE netflix;

CREATE TABLE countries (
    id VARCHAR(10),
    name VARCHAR(200),
    
    PRIMARY KEY (id)
)  ENGINE=INNODB AUTO_INCREMENT=0 DEFAULT CHARSET=UTF8;

INSERT INTO countries (id,name) values
('1','Spain'),('2','USA'),('3','UK');


CREATE TABLE actors (
    id VARCHAR(10),
    name VARCHAR(200),
    surname VARCHAR(200),
    birth DATE,
    countryId VARCHAR(10),
    
    
    PRIMARY KEY (id),
    FOREIGN KEY (countryId) REFERENCES countries(id) 
)  ENGINE=INNODB AUTO_INCREMENT=0 DEFAULT CHARSET=UTF8;

INSERT INTO actors (id,name,surname,birth,countryId) VALUES
('1','Brad', 'Pitt', '1960-12-01','2'),
('2','Angelina', 'Jolie', '1962-12-01','2'),
('3','Javier', 'Bardem', '1958-12-01','1');
