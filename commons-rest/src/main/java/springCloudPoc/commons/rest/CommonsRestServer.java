package springCloudPoc.commons.rest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

/**
 * Created by bayona on 30/08/2016.
 */
@EnableAutoConfiguration
@EnableDiscoveryClient
@SpringBootApplication
public class CommonsRestServer {

    public static void main(String[] args) {
        SpringApplication.run(CommonsRestServer.class, args);
    }
}
