package springCloudPoc.commons.rest.countries.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springCloudPoc.commons.rest.countries.controllers.dto.CountryDto;
import springCloudPoc.commons.rest.countries.services.api.ICountriesService;

import java.util.List;

/**
 * Created by bayona on 30/08/2016.
 */
@RestController
public class CountriesController {

    @Autowired
    private ICountriesService countriesService;

    @RequestMapping(value = "/{id}" , method = RequestMethod.GET)
    CountryDto getCountry(@PathVariable String id){
        return countriesService.getCountry(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    List<CountryDto> listCountries(){
        return countriesService.listCountries();
    }



}
