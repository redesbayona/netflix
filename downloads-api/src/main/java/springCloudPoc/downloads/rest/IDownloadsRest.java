package springCloudPoc.downloads.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springCloudPoc.downloads.rest.dto.DownloadDto;

/**
 * Created by bayona on 30/08/2016.
 */
@RestController
public interface IDownloadsRest {

    @RequestMapping(method = RequestMethod.GET)
    DownloadDto getDownload();
}
