package springCloudPoc.countries.countries.services.mappers;

import org.springframework.stereotype.Component;
import springCloudPoc.countries.countries.repositories.models.CountryModel;
import springCloudPoc.countries.rest.dto.CountryDto;
import springCloudPoc.webUtils.commons.mapper.IMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bayona on 30/08/2016.
 */
@Component
public class CountryMapper implements IMapper<CountryDto, CountryModel> {

    public CountryDto mapToOuter(final CountryModel inner) {
        if (inner == null) {
            return null;
        }
        CountryDto outer = new CountryDto();
        outer.setId(inner.getId());
        outer.setName(inner.getName());
        return outer;
    }

    public CountryModel mapToInner(final CountryDto outer) {
        if (outer == null) {
            return null;
        }
        CountryModel inner = new CountryModel();
        inner.setId(outer.getId());
        inner.setName(outer.getName());
        return inner;
    }

    public List<CountryDto> mapToOuterList(final List<CountryModel> innerList) {
        if (innerList == null) {
            return null;
        }
        List<CountryDto> outerList = new ArrayList<>();
        for (CountryModel countryModel : innerList) {
            outerList.add(mapToOuter(countryModel));
        }
        return outerList;
    }

    public List<CountryModel> mapToInnerList(final List<CountryDto> outerList) {
        if (outerList == null) {
            return null;
        }
        List<CountryModel> innerList = new ArrayList<>();
        for (CountryDto countryDto : outerList) {
            innerList.add(mapToInner(countryDto));
        }
        return innerList;
    }
}
