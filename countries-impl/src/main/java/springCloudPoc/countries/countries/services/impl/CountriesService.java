package springCloudPoc.countries.countries.services.impl;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springCloudPoc.countries.countries.repositories.ICountriesRepository;
import springCloudPoc.countries.countries.repositories.models.CountryModel;
import springCloudPoc.countries.countries.services.api.ICountriesService;
import springCloudPoc.countries.countries.services.mappers.CountryMapper;
import springCloudPoc.countries.rest.dto.CountryDto;

import java.util.List;

/**
 * Created by bayona on 30/08/2016.
 */
@Service
public class CountriesService implements ICountriesService {

    @Autowired
    private ICountriesRepository countriesRepository;

    @Autowired
    private CountryMapper countryMapper;

    @Override
    public CountryDto getCountry(String id) {
        CountryModel one = countriesRepository.findOne(id);
        return countryMapper.mapToOuter(one);
    }

    @Override
    public List<CountryDto> listCountries() {
        Iterable<CountryModel> all = countriesRepository.findAll();
        return countryMapper.mapToOuterList(IteratorUtils.toList(all.iterator()));
    }

    @Override
    public void addCountries(List<CountryDto> countries) {
        List<CountryModel> countryModels = countryMapper.mapToInnerList(countries);
        countriesRepository.save(countryModels);
    }
}
