package springCloudPoc.countries.countries.services.api;

import springCloudPoc.countries.rest.dto.CountryDto;

import java.util.List;

/**
 * Created by bayona on 30/08/2016.
 */
public interface ICountriesService {

    CountryDto getCountry(String id);

    List<CountryDto> listCountries();

    void addCountries(List<CountryDto> countries);
}
