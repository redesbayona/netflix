package springCloudPoc.countries.countries.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springCloudPoc.countries.countries.services.api.ICountriesService;
import springCloudPoc.countries.rest.ICountriesRest;
import springCloudPoc.countries.rest.dto.CountryDto;

import java.util.List;

/**
 * Created by bayona on 30/08/2016.
 */
@RestController
public class CountriesRest implements ICountriesRest {

    @Autowired
    private ICountriesService countriesService;

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CountryDto getCountry(@PathVariable String id) {
        return countriesService.getCountry(id);
    }

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public List<CountryDto> listCountries() {
        return countriesService.listCountries();
    }


}
