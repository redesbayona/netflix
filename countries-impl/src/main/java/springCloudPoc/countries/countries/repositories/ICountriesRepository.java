package springCloudPoc.countries.countries.repositories;

import org.springframework.data.repository.CrudRepository;
import springCloudPoc.countries.countries.repositories.models.CountryModel;

/**
 * Created by bayona on 30/08/2016.
 */
public interface ICountriesRepository extends CrudRepository<CountryModel, String> {

}
