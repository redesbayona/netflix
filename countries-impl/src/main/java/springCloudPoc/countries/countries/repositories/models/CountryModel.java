package springCloudPoc.countries.countries.repositories.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by bayona on 30/08/2016.
 */
@Data
@Entity
@Table(name = "countries")
public class CountryModel {

    @Id
    @GeneratedValue
    private String id;
    private String name;
}
