package springCloudPoc.countries;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created by bayona on 30/08/2016.
 */
@EnableAutoConfiguration
@EnableDiscoveryClient
@SpringBootApplication
public class CountriesServer {

    public static void main(String[] args) {
        SpringApplication.run(CountriesServer.class, args);
    }
}
