package springCloudPoc.downloads.services.api;

import springCloudPoc.downloads.rest.dto.DownloadDto;

/**
 * Created by bayona on 30/08/2016.
 */
public interface IDownloadsService {

    DownloadDto getDownload();
}
