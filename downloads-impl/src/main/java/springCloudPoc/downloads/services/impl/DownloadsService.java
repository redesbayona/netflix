package springCloudPoc.downloads.services.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import springCloudPoc.downloads.rest.dto.DownloadDto;
import springCloudPoc.downloads.services.api.IDownloadsService;

import java.util.Date;

/**
 * Created by bayona on 30/08/2016.
 */
@Service
public class DownloadsService implements IDownloadsService {

    public static final transient Logger log = Logger.getLogger(DownloadsService.class);

    @Override
    @HystrixCommand(fallbackMethod = "getDownloadLowQuality")
    public DownloadDto getDownload() {
        Date date = new Date();
        if (date.getTime() % 2 == 0) {
            throw new RuntimeException("Shit happens");
        }
        DownloadDto downloadDto = new DownloadDto();
        downloadDto.setFile("new file");
        return downloadDto;
    }

    @HystrixCommand
    public DownloadDto getDownloadLowQuality() {
        log.error("Fallback method init");
        return new DownloadDto();
    }
}
