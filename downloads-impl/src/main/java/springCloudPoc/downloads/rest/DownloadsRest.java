package springCloudPoc.downloads.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springCloudPoc.downloads.rest.dto.DownloadDto;
import springCloudPoc.downloads.services.api.IDownloadsService;

/**
 * Created by bayona on 30/08/2016.
 */
@RestController
public class DownloadsRest implements IDownloadsRest {

    @Autowired
    private IDownloadsService downloadsService;

    @RequestMapping(method = RequestMethod.GET)
    public DownloadDto getDownload() {
        return downloadsService.getDownload();
    }

}
