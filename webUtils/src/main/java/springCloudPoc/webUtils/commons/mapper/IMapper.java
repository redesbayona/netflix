package springCloudPoc.webUtils.commons.mapper;

import org.apache.commons.lang.NotImplementedException;

import java.util.List;

/**
 * Created by bayona on 29/08/2016.
 */
public interface IMapper<T, S> {

    default T mapToOuter(final S inner) {
        throw new NotImplementedException();
    }

    default S mapToInner(final T outer) {
        throw new NotImplementedException();
    }

    default List<T> mapToOuterList(final List<S> innerList) {
        throw new NotImplementedException();
    }

    default List<S> mapToInnerList(final List<T> outerList) {
        throw new NotImplementedException();
    }

}
